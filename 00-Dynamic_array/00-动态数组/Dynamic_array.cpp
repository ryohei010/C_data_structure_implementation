﻿#include"Dynamic_array.h"
#include<iostream>
using namespace std;
const int DEFT_CAPACITY = 5;
 int FLAG = 0;
//Dy_Array::Dy_Array() {
//	m_capacity = DEFT_CAPACITY;
//	m_data = new int[m_capacity];
//}
Dy_Array::Dy_Array(int capacity) {
	if (capacity<=0)
	{
		m_capacity = DEFT_CAPACITY;

	}
	else
	{
		m_capacity = capacity;
	}
	m_data = new int[m_capacity];
}

Dy_Array::~Dy_Array() {
	if (m_data == nullptr) return ;
	delete[] m_data;
	m_data = nullptr;
}
void Dy_Array::add(int value) {
	if (m_size==m_capacity)//检测动态数组是否已满
	{
		
		int* new_m_data = new int[m_capacity*2];
		for (int i = 0; i < m_size; i++) {
			new_m_data[i] = m_data[i];
		}
		delete[] m_data;
		m_data = new_m_data;
		int old = m_capacity;
		m_capacity = m_capacity * 2;
		cout << "动态数组已经扩容:" << "由" << old << "扩容到" << m_capacity << endl;
	}
	m_data[m_size] = value;
	m_size++;
}

int Dy_Array::get(int index) {
	if (index < 0 || index >= m_size)
	{
		throw "索引越界";
	}
	return m_data[index];
}
  int Dy_Array::size() {
	  cout << "size:";
	return m_size;
}
 int Dy_Array::operator[](int index){
	return get(index);
}
 ostream& operator<<(ostream& cout,  const Dy_Array& array) {
	
	 cout << "[";
	 for (int  i = 0; i <array.m_size ; i++)
	 {
		 if (i != 0) {
			 cout << ",";
		 }
		 cout << array.m_data[i];
	 }
	 return cout << "]";
 }
 void Dy_Array::insert(int index, int value) {
	 if (index < 0 || index > m_size)//可以向数组最后面插入值
	 {
		 throw "索引越界";
	 }
	 for (int i = m_size-1; i >= index; i--)
	 {
		 m_data[ i+1]= m_data[i];

	 }
	 m_data[index] = value;
	 m_size++;
 }
 void Dy_Array::remove(int index) {
	 if (index < 0 || index >= m_size)//可以向数组最后面插入值
	 {
		 throw "索引越界";
	 }
	 for (int i = index; i < m_size-1; i++)
	 {
		 m_data[i] = m_data[i + 1];
	 }
	 m_size--;
 }

 void Dy_Array::index_find(int value) {
	 
	 for (int i = 0; i < m_size; i++)
	 {
		 if (m_data[i]==value)
		 {
			 cout << "该元素位于：" << i << endl;
			 FLAG = 1;
		 }
	 }
	 if (FLAG== 0) {
		 cout << "该元素不存在动态数组中" << endl;
	 }
	 
 }