﻿#pragma once
using namespace std;
#include<iostream>
class Dy_Array {
private:
	int* m_data;//指向动态数组的首个元素
	int m_size=0;//元素个数
	int m_capacity;//数组大小
	friend ostream& operator<<(ostream& cout, const Dy_Array& array);//运算符重写
public:
	/*Dy_Array();*/
	Dy_Array(int capacity=0);
	~Dy_Array();
	int get(int  index);//指定获得某个位置的元素
	void add(int value);//增加元素
	 int size();//获得元素个数
	 void remove(int index);//删除指定位置元素
	 void insert(int index, int value);//插入指定位置元素
	int operator[](int index);//运算符重写
	void index_find(int value);
};

