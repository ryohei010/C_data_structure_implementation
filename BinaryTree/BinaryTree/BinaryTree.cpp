﻿#include "BinaryTree.h"
#include"Node.h"
using namespace std;
#include<iostream>
#include<queue>
#include <math.h>
Node* BinaryTree::predecessor(Node* node) {//求前驱节点
	if (node == nullptr) return nullptr;
	//前驱节点在左子树中（left->right->right.....）
	Node* p = node->left;
	if (p != nullptr) {
		while (p->right != nullptr) {
			p = p->right;
		}
		return p;
	}
	//不存在左子树，前驱节点在父节点、祖父节点中
	while (node->parent!=nullptr&&node==node->left) {
		node = node->parent;
	}
	//node->parent==nullptr第一种情况
	//node==node->right 第二种情况
	return node->parent;
}

Node* BinaryTree::successor(Node* node) {//求后继
	if (node==nullptr)
	{
		return nullptr;
	}

	Node* p = node->right;
	if (p!=nullptr)
	{
		while (p->left!=nullptr)
		{
			p = p->left;
		}
		return p;
	}

	while (node->parent != nullptr && node == node->right) {
		node = node->parent;
	}
	return node->parent;
}
int BinaryTree::height(Node* node) {
	if (node == nullptr) return 0;
	return 1 + int(fmax(height(node->left), height(node->right)));
}
int BinaryTree::height() {
	return height(root);
}
BinaryTree::BinaryTree() {

	cout << "create BinaryTree success! " << endl;
}
void BinaryTree::preorderTraversal() {
	preorderTraversal(root);
}
void BinaryTree::preorderTraversal(Node* node) {//前序遍历
	if (node==nullptr)
	{
		return;
	}
	cout << node->m_data << " ";
	preorderTraversal(node->left);
	preorderTraversal(node->right);
}
void BinaryTree::inorderTraversal() {
	inorderTraversal(root);
}
void BinaryTree::inorderTraversal(Node* node) {//后序遍历
	if (node == nullptr)
	{
		return;
	}
	inorderTraversal(node->left);
	cout << node->m_data << " ";
	inorderTraversal(node->right);
}
void BinaryTree::afterorderTraversal() {
	afterorderTraversal(root);
}
void BinaryTree::afterorderTraversal(Node*node) {//后序遍历
	if (node==nullptr)
	{
		return;
	}
	afterorderTraversal(node->left);
	afterorderTraversal(node->right);
	cout << node->m_data << " ";
}
void BinaryTree::levelorderTraversal() {
	if (root == nullptr) return;
	queue<Node*> q;
	q.push(root);
	while (!q.empty())
	{
		Node* node = q.front();
		q.pop();
		cout << node->m_data << " ";
		if (node->left!=nullptr)
		{
			q.push(node->left);
		}
		if (node->right!=nullptr)
		{
			q.push(node->right);
		}
	}
}
BinarySearchTree::BinarySearchTree() {
	cout << "create BinarySearchTree success! " << endl;
}
int BinarySearchTree::size() {

	return m_size;
}

bool BinarySearchTree::isEmpty() {
	return m_size == 0;
}

void BinarySearchTree::dataNotNullcheck(int data) {
		if (data==NULL)
	{
			throw "data must not be null";
	}
}
int BinarySearchTree::compare(int data1, int data2) {
	if (data1==data2)
	{
		return 0;
	}
	else if(data1<data2)
	{
		return -1;
	}
	else if (data1 > data2) {
		return 1;
	}
}
void BinarySearchTree::add(int data) {
	dataNotNullcheck(data);

	if (root==nullptr)//添加的是第一个元素！
	{
		root = new Node(data, nullptr);
		m_size++;
		return;
	}
	//添加的不是第一个元素！
	//第一步:找到父节点！
	Node* parent = root;
	Node* node = root;
	int cmp = 0;
	do
	{
		cmp = compare(data, node->m_data);
		parent = node;
		if (cmp>0)
		{
			node = node->right;
		}
		else if(cmp<0)
		{
			node = node->left;
		}
		else
		{
			node->m_data = data;
			return;
		}
	} while (node!=nullptr);

	//第二步:看看插入到父节点的哪个位置！
	Node* newNode = new Node(data, parent);
	if (cmp>0)
	{
		parent->right = newNode;
	}
	else
	{
		parent->left = newNode;
	}
	m_size++;
}


