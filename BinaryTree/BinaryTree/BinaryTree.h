﻿#pragma once
#include "Node.h"
#include <math.h>
class BinaryTree
{
protected:
	//前序遍历！
	void preorderTraversal(Node* node);
	//中序遍历！
	void inorderTraversal(Node* node);
	//后序遍历(递归)
	void afterorderTraversal(Node* node);
	/*void levelorderTraversal(Node* node);*/
	int m_size;
	Node* root;
	int height(Node* node);//获取某个节点的高度
	Node* predecessor(Node* node);//获取某个节点的前驱节点
	Node* successor(Node* node);//获取某个节点的后继节点
public:
	virtual int size()=0;
	virtual bool isEmpty()=0;
	/*virtual void clear()=0;*/
	virtual void add(int data)=0;
	/*virtual void remove(int data)=0;
	virtual bool contains(int data)=0;*/
	BinaryTree();
	void preorderTraversal();
	void inorderTraversal();
	void afterorderTraversal();
	//层序遍历
	void levelorderTraversal();
	int height();//获取二叉树的高度
};

class BinarySearchTree :public BinaryTree {

public:
	int size();
	bool isEmpty();
	/*void clear();*/
	void add(int data);
	/*void remove(int data);
	bool contains(int data);*/
	BinarySearchTree();
	void dataNotNullcheck(int data);
	//元素之间的比较，返回0代表相等，返回1代表data1>data2，返回-1代表data1<data2；
	int compare(int data1, int data2);
};


