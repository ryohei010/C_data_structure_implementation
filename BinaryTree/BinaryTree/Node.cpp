﻿#include "Node.h"

Node::Node(int data, Node* parent) :m_data(data), parent(parent) {};

bool Node::hastwochildren() {
	return left != nullptr && right != nullptr;
}
bool Node::isleaf() {

	return left == nullptr && right == nullptr;
}