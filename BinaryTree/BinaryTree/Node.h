﻿#pragma once
class Node
{
public:
	int  m_data;
	Node* left;
	Node* right;
	Node* parent;
	Node(int data, Node* parent);
	bool isleaf();
	bool hastwochildren();

};

