﻿#include<iostream>
#include"BinaryTree.h"
#include"Node.h"

using namespace std;

int main() {

	BinarySearchTree* bst = new BinarySearchTree;
	bst->add(31);
	bst->add(45);
	bst->add(44);
	bst->add(33);
	bst->add(73);
	bst->add(42);
	bst->add(56);
	cout << "preorderTraversal:" << " ";
	bst->preorderTraversal();
	cout << endl;
	cout << "inorderTraversal:" << " ";
	bst->inorderTraversal();
	cout << endl;
	cout << "levelorderTraversal:" << " ";
	bst->levelorderTraversal();
	cout << endl;
	cout << bst->height() << endl;
	getchar();
	return 0;
}