﻿#include"DoublyLinkedList.h"
Node::Node(Node* prev, int data, Node* next) :prev(prev), m_data(data), next(next) {};

//DoublyLinkedList::DoublyLinkedList() {
//	head = nullptr;
//	last = nullptr;
//	m_size = 0;
//}

void DoublyLinkedList::rangeCheck(int index) {
	if (index < 0 || index >=m_size) {
		throw"索引越界";
	}
}
void DoublyLinkedList::rangeCheckForAdd(int index) {
	if (index < 0 || index > m_size)
	{
		throw"索引越界";
	}
}
//
Node* DoublyLinkedList::node(int index) {
	rangeCheck(index);
	if (index < (m_size >> 1)) {
		Node* node = head;
		for (int i = 0; i < index; i++)
		{
			node = node->next;
		}
		return node;
	}
	else
	{
		Node* node = last;
		for (int i = m_size-1; i > index; i--)
		{
			node = node->prev;
		}
		return node;
	}

}

void DoublyLinkedList::clear() {
	Node* p;
	for (int i = 0; i < m_size; i++)
	{
		p = head;
		head = head->next;
		delete p;
	}
	head = nullptr;
	last = nullptr;
	m_size = 0;
}

int DoublyLinkedList::get(int index) {
	rangeCheck(index);
	Node* p=node(index);
	return p->m_data;

}
int DoublyLinkedList::size() {
	return m_size;
}

int DoublyLinkedList::set(int index, int element) {
	rangeCheck(index);
	Node* p = node(index);
	int olddata = p->m_data;
	p->m_data = element;
	return olddata;
}

void DoublyLinkedList::add(int index, int element) {
	rangeCheckForAdd(index);
	if (index==m_size)
	{
		add(element);
	}
	else
	{
		Node* next = node(index);
		Node* prev = next->prev;
		Node* node = new Node(prev, element, next);
		next->prev = node;//后结点的prev连线
		if (prev==nullptr)//index==0,前结点的next连线
		{
			head = node;
		}
		else
		{
			prev->next = node;
		}
		m_size++;
	}
}

void DoublyLinkedList::add(int element) {
	Node* oldlast = last;
	last = new Node(oldlast, element, nullptr);
	if (oldlast==nullptr)//这是双向链表添加的第一个元素
	{
		head = last;
	}
	else {
		oldlast->next = last;
	}
	m_size++;
}
int DoublyLinkedList::remove(int index) {
	rangeCheck(index);
	Node* prev = nullptr;
	Node* next = nullptr;
	Node* Node = node(index);
	prev = Node->prev;
	next = Node->next;
	if (prev==nullptr)//index==0,删除第一个结点
	{
		head = next;
		next->prev = nullptr;
	}
	else
	{
		prev->next = next;
	}

	if (next == nullptr) {
		last = prev;
		prev->next = nullptr;
	}
	else
	{
		next->prev = prev;
	}
	m_size--;
	return Node->m_data;
	delete Node;
}

int DoublyLinkedList::indexof(int element) {
	Node* node = head;
	for (int  i = 0; i < m_size; i++)
	{
		if (element == node->m_data) return i;
		node = node->next;
	}
	return ELEMENT_NOT_FOUND;
}