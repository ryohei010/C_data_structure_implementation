﻿#pragma once
int const ELEMENT_NOT_FOUND = -1;
class Node {
	/*Node* prev;
	Node* next;
	int m_data;*/
public:
	Node* prev;
	Node* next;
	int m_data;
	Node(Node* prev, int data, Node* next);
};

class DoublyLinkedList:public Node{
	Node* head;
	Node* last;
	int m_size;
public:
	DoublyLinkedList() :Node(nullptr, 0, nullptr) {
		head = nullptr;
		last = nullptr;
		m_size = 0;
	}
	void rangeCheck(int index);
	void rangeCheckForAdd(int index);
	void clear();
	int get(int index);
	int set(int index, int element);
	void add(int index, int element);
	void add(int element);//在尾部添加元素
	int remove(int index);
	int indexof(int element);
	int size();
	Node* node(int index);
};
