﻿#include<iostream>
using namespace std;
//递归实现
int fbi1(int n) {

	if (n <= 1) return n;
	return fbi1(n - 1) + fbi1(n - 2);

}
//for循环实现
int fbi2(int n) {
	if (n <= 1) return n;
	int first = 0;
	int second = 1;
	for (int i = 0; i < n-1; i++)
	{
		
		int sum = first + second;
		first = second;
		second = sum;
	
	}
	return second;



}



int main() {
	cout << fbi2(0) << endl;
	cout << fbi2(1) << endl;
	cout << fbi2(2) << endl;
	cout << fbi2(3) << endl;
	cout << fbi2(4) << endl;

	
	getchar();
	return 0;
}