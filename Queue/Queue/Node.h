﻿#pragma once
class Node {
	Node* prev;
	int m_data;
	Node* next;
	friend class Queue;
public:
	Node(Node* prev, int element, Node* next);
};

