﻿#include"Node.h"
#include"Queue.h"
#include<iostream>
using namespace std;


Queue::Queue() {
	m_size = 0;
	head = nullptr;
	tail = nullptr;
}

int Queue::size() {
	return m_size;
}

bool Queue::isEmpty() {
	if (m_size==0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Queue::rangcheck(int index) {

	if (index<0||index>=m_size)
	{
		throw"索引越界";
	}
}

void Queue::rangcheckforAdd(int index) {

	if (index < 0 || index >m_size)
	{
		throw"索引越界";
	}
}

Node* Queue::node(int index) {
	rangcheck(index);
	Node* Node=nullptr;
	if (index<(m_size>>1))
	{
		for (int i = 0; i < index; i++)
		{
			Node = Node->next;
		}
	}
	else
	{
		for (int i = m_size; i > index; i--)
		{
			Node = Node->prev;
		}
	}
	return Node;

}

void Queue::enQueue(int element) {

	if (m_size==0)//添加的第一个元素
	{
		head = new Node(nullptr, element, nullptr);
		tail = head;
	}
	else
	{
		Node* oldtail = tail;
		tail = new Node(oldtail, element, nullptr);
		oldtail->next = tail;
	}
	m_size++;
}

int Queue::deQueue() {
	Node* temp = nullptr;
	temp = head;
	int data;
	if (m_size == 0) {
		cout << "队伍已经出队完成" << endl;
		return 0;
	}
	else if(m_size==1)//只剩最后一个元素
	{

		 data=temp->m_data;
		 head = tail = nullptr;
	}
	else if(m_size>1)
	{
		data=temp->m_data;
		head = temp->next;
	}
	m_size--;
	delete temp;
	return data;
}

int Queue::front() {
	return head->m_data;
}