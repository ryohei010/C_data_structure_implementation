﻿#pragma once
#pragma once
#include"Node.h"
class Queue {
	int m_size;
	Node* head;
	Node* tail;
public:
	Queue();
	int size();
	void enQueue(int element);//入队
	int deQueue();//出队
	int front();//获取队列头元素
	void clear();
	bool isEmpty();
	void rangcheck(int index);
	void rangcheckforAdd(int index);
	Node* node(int index);
};

