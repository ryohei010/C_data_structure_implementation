#include"linklist.h"
#include<iostream>
using namespace std;
Node::Node(int data, Node* next) :m_data(data),next(next){}
void linklist::rangCheck(int index) {
	if (index < 0 || index >= m_size) {
		throw "索引越界";
	}
}
void linklist::rangCheckadd(int index) {
	if (index < 0 || index > m_size) {
		throw "索引越界";
	}
}

Node* linklist::node(int index) {
		rangCheck(index);
		Node* node = head;
		for (int  i = 0; i < index; i++)
		{
			node = node->next;
		}
		return node;
}

void linklist::add(int index, int data) {
	rangCheckadd(index);
	if (index == 0) {
		head = new Node(data, head);
	}
	else
	{
		Node* prev = node(index - 1);
		prev->next = new Node(data, prev->next);
	}
	m_size++;

}

int linklist::get(int index) {
	Node* p=node(index);

	return p->m_data;
	

}
int linklist::size() {
	return m_size;
}

void linklist::remove(int index) {
	rangCheck(index);
	Node* temp=nullptr;
	if (index == 0) {
		temp = node(index);
		head = temp->next;
	}
	else {
		Node* prev = node(index - 1);
		Node* temp = prev->next;
		prev->next = prev->next->next;
	}
	delete temp;
	m_size--;
}

void linklist::clear() {
	Node* temp = nullptr;
	for (int i = 0; i < m_size; i++)
	{
		if (head != nullptr) {
			temp = head;
			head = head->next;
			delete temp;
		}
	}
	head = nullptr;
}



 



