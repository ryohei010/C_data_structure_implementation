﻿#include<iostream>
using namespace std;

class Car {
private:
	Car(){}
	~Car(){}
	int m_price;
	static Car* c;

public:
	static Car *sharecar() {

		if (c == NULL) {
			c = new Car();
		}
		return c;
	}
	static void deleterCar() {
		if (c != NULL) {
			delete c;
			c = NULL;
			cout << "该对象已被删除" << endl;
		}
	}
	void setPrice(int price)  {
		m_price = price;
	}
	int getPrice() {
		return m_price;
	}
	/*Car(int price) :m_price(price) {

	}*/
};
Car* Car::c = NULL;
int main() {
	Car* c1 = Car::sharecar();
	c1->setPrice(10);
	
	cout << c1->getPrice() << endl;
	c1->deleterCar();

	

	getchar();
	return 0;
}