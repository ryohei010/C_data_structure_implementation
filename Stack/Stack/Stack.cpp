﻿#include"Node.h"
#include"Stack.h"
#include<iostream>
using namespace std;

Stack::Stack() {
	m_size = 0;
	head = nullptr;
	tail = nullptr;
}

int Stack::size() {
	return m_size;
}
void Stack::rangcheck(int index) {
	if (index<0||index>=m_size)
	{
		throw"索引越界";
	}
}
void Stack::rangcheckforAdd(int index) {
	if (index < 0 || index >=m_size)
	{
		throw"索引越界";
	}
}

Node* Stack::node(int index) {
	rangcheck( index);
	if (index<(m_size>>1))
	{
		Node* node = head;
		for (int i = 0; i < index; i++)
		{
			node = node->next;
		}
		return node;
	}
	else
	{
		Node* node = tail;
		for (int i = m_size-1; i >index; i--)//要从最后一个元素的索引开始，即m_szie-1；
		{
			node = node->prev;
		}
		return node;
	}
}

void Stack::push(int element) {
	
	if (m_size == 0) {
		tail = new Node(head, element, tail);
		head= tail;
	}
	else
	{
		Node* oldtail = tail;
		tail = new Node(oldtail, element, nullptr);
		oldtail->next = tail;
	}
	m_size++;

}

int Stack::top() {
	return tail->m_data;
}

int Stack::pop() {
	int data;
	if (m_size==0)
	{
		return 0;
	}
	Node* oldtail = tail;
	if (m_size == 1) {
		tail = oldtail->prev;
		head = tail;
		data= oldtail->m_data;
		m_size--;
	}
	else if(m_size>1)
	{
		tail = oldtail->prev;
		data= oldtail->m_data;
		m_size--;
	}
	delete oldtail;
	return data;
}


void Stack::clear() {

	for (int  i = 0; i < m_size; i++)
	{
		Node* p = head;
		head = head->next;
		delete p;
	}
	head = tail = nullptr;
	m_size = 0;
	cout << "栈已经被清空" << endl;
}