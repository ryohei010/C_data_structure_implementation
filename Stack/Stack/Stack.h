﻿#pragma once
#include"Node.h"
class Stack {
	int m_size;
	Node* head;
	Node* tail;

public:
	Stack();
	int size();
	void push(int element);//入栈
	int pop();
	int top();
	void clear();
	bool isEmpty();
	void rangcheck(int index);
	void rangcheckforAdd(int index);
	Node* node(int index);
};
