#pragma once
#pragma once
class Node {
	Node* prev;
	int m_data;
	Node* next;
	friend class Stack;
public:
	Node(Node* prev, int element, Node* next);
};

