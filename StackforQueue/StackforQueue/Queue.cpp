#include"Queue.h"
#include<iostream>
using namespace std;
int Queue::size() {


	return inStack.size() + outStack.size();

}

void Queue::enQueue(int element) {
	inStack.push(element);
}

int Queue::deQueue() {
	if (isEmpty())
	{
		cout << "队伍为空！请先添加成员！错误代码：";
		return QUEUE_IS_EMPTY;
	}
	else
	{
		if (outStack.isEmpty())
		{
			while (!inStack.isEmpty())
			{
				outStack.push(inStack.pop());
			}
		}
	}
	cout << "出队元素为：";
	return outStack.pop();
}

int Queue::front() {
	if (isEmpty())
	{
		cout << "队伍为空！请先添加成员！错误代码：";
		return QUEUE_IS_EMPTY;
	}
	else
	{
		if (outStack.isEmpty())
		{
			while (!inStack.isEmpty())
			{
				outStack.push(inStack.pop());
			}
		}
	}
	cout << "队头元素为：";
	return outStack.top();
}

bool Queue::isEmpty() {
	if (outStack.isEmpty()&&inStack.isEmpty())
	{
		return true;
	}
	else
	{
		return false;
	}
}