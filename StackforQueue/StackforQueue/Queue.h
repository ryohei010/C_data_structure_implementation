#pragma once
#include"Stack.h"
const int QUEUE_IS_EMPTY = -1;
class Queue {
	Stack inStack;
	Stack outStack;
public:
	/*Queue();*/
	int size();
	void enQueue(int element);//入队
	int deQueue();//出队
	int front();//获取队列头元素
	void clear();
	bool isEmpty();
	/*void rangcheck(int index);
	void rangcheckforAdd(int index);*/
	/*Node* node(int index);*/
};
